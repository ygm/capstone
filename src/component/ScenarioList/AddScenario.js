import React from 'react';
import {Card, CardBody, Col} from "reactstrap";
import AddModal from "./AddModal";

const AddScenario = (props) => {
    const {handleChange, handleInsert, addModalState} = props;

    return (
      <Col sm="3">
        <Card body className="CardFont CardBtn">
          <CardBody className="CardBody">
            <AddModal
              handleChange = {handleChange}
              handleInsert = {handleInsert}
              addModalState = {addModalState}
            />
          </CardBody>
        </Card>
      </Col>
    );
};

export default AddScenario;
