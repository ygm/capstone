import React, {Component} from 'react';
import {Button, Modal, ModalBody, ModalFooter, ModalHeader} from "reactstrap";
import {ScenarioActions} from "../../store/actionCreators";

class AddModal extends Component {

    toggle = () => {
        const {addModalState} = this.props;

        if (addModalState) {
            ScenarioActions.addModalClose();
        } else {
            ScenarioActions.addModalOpen();
        }
    };

    render() {
        const { handleChange, handleInsert, addModalState } = this.props;
        return (
          <div>
              <Button onClick={this.toggle}
                // style={this.bStyle}
                      className="AddButton">+</Button>
              <Modal isOpen={addModalState} toggle={this.toggle}
                // className={this.props.className}
              >
                  <ModalHeader toggle={this.toggle}>
                      시나리오 추가
                  </ModalHeader>
                  <ModalBody>
                      <input
                        type="text"
                        onChange={handleChange}
                        placeholder={"시나리오 이름"}
                      />
                  </ModalBody>
                  <ModalFooter>
                      <Button onClick={handleInsert}>추가</Button>{' '}
                      <Button color="secondary" onClick={this.toggle}>취소</Button>
                  </ModalFooter>
              </Modal>
          </div>
        );
    }
}

export default AddModal;