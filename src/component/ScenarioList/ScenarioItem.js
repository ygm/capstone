import React from 'react';
import {Button, Card, CardTitle, Col} from "reactstrap";

const ScenarioItem = (props) => {
    const { handleRemove, goBoard, name} = props;
    // const bStyle = {
    //     margin: "3px"
    // };
    return (
      <Col sm="3">
          <Card body className={"CardFont"}>
              <CardTitle className={"CardTitle"}>{name.name}</CardTitle>
              <Button onClick={()=> goBoard(name.name, name._id)}
                // style={this.bStyle}
              >Go</Button>
              <Button onClick={() => handleRemove(name._id)}
                // style={this.bStyle}
              >삭제</Button>
          </Card>
      </Col>
    );
};

export default ScenarioItem;