import React from 'react';
import {Row} from "reactstrap";
import "../../styles/card.css";
import AddScenario from "./AddScenario";
import ScenarioItem from "./ScenarioItem";

const ScenarioListItem = (props) => {

    const {scenarios, handleChange, handleInsert, addModalState, goBoard, handleRemove} = props;

    const nstyle = {
        margin: "15vh 5%"
    };

    const nameList = scenarios.map(
        (name, index) => (
          <ScenarioItem
            key = {index}
            goBoard = {goBoard}
            handleRemove = {handleRemove}
            name = {name}
          />
        )
    );


    return (
        <Row
            style={nstyle}
        >
            {nameList}
            <AddScenario
                handleChange = {handleChange}
                handleInsert = {handleInsert}
                addModalState = {addModalState}
            />
        </Row>
    );

};

export default ScenarioListItem;
