import React from 'react';
import FormGroup from "reactstrap/es/FormGroup";
import Input from "reactstrap/es/Input";
import Button from "reactstrap/es/Button";
import Form from "reactstrap/es/Form";
import "./SignUpForm.css";

const SignUpForm = (props) => {
    const {  signUp, toSignInPage} = props;
    return (
        <Form action="" className="signUpForm" onSubmit={signUp}>
            <div className="SignUp__body">
                {/*<div className="text-center text">회원가입</div>*/}

                <FormGroup className="form__field">
                    <label htmlFor="id">ID</label>

                    <Input
                        type="text"
                        name="username"
                        id="id"
                        placeholder="ID"
                    />

                </FormGroup>
                <FormGroup className="form__field">
                    <label htmlFor="pwd">PASSWORD</label>

                    <Input
                        type="text"
                        name="password"
                        id="pwd"
                        placeholder="PASSWORD"
                    />

                </FormGroup>
            </div>
            <div className="SignUp__footer">
                <Button type="submit" size="md"
                        className="btnC">SIGN UP</Button>
                <Button type="button" onClick={toSignInPage} size="md"
                        className="btnC">BACK</Button>
            </div>

        </Form>
    );
};

export default SignUpForm;