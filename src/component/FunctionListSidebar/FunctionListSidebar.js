import React, {Component} from 'react';
import "./FunctionListSidebar.css";
import {FunctionActions} from "../../store/actionCreators";
import {withRouter} from "react-router-dom";
import {connect} from "react-redux";
import FunctionParam from "./FunctionParam";
import {UncontrolledTooltip} from "reactstrap";

class FunctionListSidebar extends Component {

    render() {
        const functionClick = (functionName) => {
            const {funParamOpen} = this.props;
            if (funParamOpen) {
                FunctionActions.funParamClose();
            } else {
                FunctionActions.funParamOpen(functionName);
            }
        };

        return (
            <nav role="navigation">
                <div className="menuToggle">
                    <input type="checkbox" checked={this.props.funListOpen? "checked":""} readOnly/>
                    <ul className="box-menu">
                        <h3>기능목록</h3>
                        <li className="function_list" >
                            <div className="function_item">
                                <img
                                    id="time-light"
                                    src={require('../../static/img/light.png')}
                                    alt=""
                                    className="function_img"
                                    onClick={() => functionClick('time-light')} />
                                <UncontrolledTooltip placement="right" target="time-light">
                                    특정 시간에 조명 on/off
                                </UncontrolledTooltip>
                            </div>
                        </li>
                        <li className="function_list" >
                            <div className="function_item">
                                <img
                                    id="time-fan"
                                    src={require('../../static/img/fan.png')}
                                    alt=""
                                    className="function_img"
                                    onClick={() => functionClick('time-fan')} />
                                <UncontrolledTooltip placement="right" target="time-fan">
                                    특정 시간에 선풍기 on/off
                                </UncontrolledTooltip>
                            </div>
                        </li>
                        <li className="function_list" >
                            <div className="function_item">
                                <img
                                    id="time-airTemp"
                                    src={require('../../static/img/airconditioner.png')}
                                    alt=""
                                    className="function_img"
                                    onClick={() => functionClick('time-airTemp')} />
                                <UncontrolledTooltip placement="right" target="time-airTemp">
                                    특정 시간에 에어컨 온도 제어
                                </UncontrolledTooltip>
                            </div>
                        </li>
                        <FunctionParam funParamState={this.props.funParamState}/>
                    </ul>
                </div>
            </nav>
        );
    }
}
export default withRouter(
    connect((state) => ({
        functionList: state.functionList.functionList,
        funParamState: state.functionList.funParamState
        })
    )(FunctionListSidebar));