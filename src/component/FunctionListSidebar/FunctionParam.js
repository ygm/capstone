import React, { Component } from 'react';
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import "./FunctionParam.css";
import { Button, Modal, ModalBody, ModalFooter, ModalHeader, Form, FormGroup } from "reactstrap";
import { BoardActions, FunctionActions, ScenarioActions } from "../../store/actionCreators";
import axios from "axios";
import { baseURL } from "../../lib/api";


class FunctionParam extends Component {

    toggle = () => {
        const { funParamState } = this.props;

        if (funParamState) {
            FunctionActions.funParamClose();
        } else {
            FunctionActions.funParamOpen();
        }
    };

    addFunction = async (e) => {
        e.preventDefault();

        const hour = e.target.hour.value;
        const deviceFunction = e.target.deviceFunction.value;
        const device = e.target.device.value;
        const functionName = e.target.functionName.value;

        try {
            const accessToken = localStorage.getItem("accessToken");

            const response = await axios.post(baseURL + '/board', {
                scenarioId: this.props.scenarioId,
                functionName: functionName,
                params: {
                    time: hour,
                    command: deviceFunction,
                    device: device
                }
            }, {
                headers: {
                    Authorization: "Bearer " + accessToken
                }
            });

            if (response.status === 200) {
                const data = {
                    functionName: functionName,
                    params: {
                        time: hour,
                        command: deviceFunction,
                        device: device
                    }
                };
                await BoardActions.functionsAdd(data);
                await FunctionActions.funParamClose();
                await BoardActions.funListClose();
                this.props.history.push('/');
                this.props.history.push('/board');

            }
        } catch (error) {
            console.log(error);
        }

    };

    modalBody = () => {
        const { paramModalFunctionName } = this.props;

        switch (paramModalFunctionName) {
            case 'time-light':
                return (
                    <ModalBody>
                        <FormGroup>
                            <input type="text" id="functionName" name="functionName"
                                value={paramModalFunctionName}
                                readOnly
                                hidden
                                required />
                        </FormGroup>
                        <FormGroup>
                            <label htmlFor="hour">시간 : </label>{' '}
                            <input type="time" id="hour" name="hour"
                                required />
                        </FormGroup>
                        <FormGroup>
                            <label htmlFor="deviceFunction">기능 : </label>{' '}
                            <select name="deviceFunction">
                                <option value="기능선택">기능선택</option>
                                <option value="on">On</option>
                                <option value="off">Off</option>
                            </select>
                        </FormGroup>
                        <FormGroup>
                            <label htmlFor="device">연결 장치 : </label>{' '}
                            <select name="device">
                                <option value="장치선택">장치선택</option>
                                <option value="장치1">장치1</option>
                                <option value="장치2">장치2</option>
                                <option value="장치3">장치3</option>
                                <option value="장치4">장치4</option>
                            </select>
                        </FormGroup>
                    </ModalBody>
                );
                break;
            case 'time-fan':
                return (
                    <ModalBody>
                        <FormGroup>
                            <input type="text" id="functionName" name="functionName"
                                value={paramModalFunctionName}
                                readOnly
                                hidden
                                required />
                        </FormGroup>
                        <FormGroup>
                            <label htmlFor="hour">시간 : </label>{' '}
                            <input type="time" id="hour" name="hour"
                                required />
                        </FormGroup>
                        <FormGroup>
                            <label htmlFor="deviceFunction">기능 : </label>{' '}
                            <select name="deviceFunction">
                                <option value="기능선택">기능선택</option>
                                <option value="on">On</option>
                                <option value="off">Off</option>
                            </select>
                        </FormGroup>
                        <FormGroup>
                            <label htmlFor="device">연결 장치 : </label>{' '}
                            <select name="device">
                                <option value="장치선택">장치선택</option>
                                <option value="장치1">장치1</option>
                                <option value="장치2">장치2</option>
                                <option value="장치3">장치3</option>
                                <option value="장치4">장치4</option>
                            </select>
                        </FormGroup>
                    </ModalBody>
                )
                break;
            case 'time-airTemp':
                return (
                    <ModalBody>
                        <FormGroup>
                            <input type="text" id="functionName" name="functionName"
                                value={paramModalFunctionName}
                                readOnly
                                hidden
                                required />
                        </FormGroup>
                        <FormGroup>
                            <label htmlFor="hour">시간 : </label>{' '}
                            <input type="time" id="hour" name="hour"
                                required />
                        </FormGroup>
                        <FormGroup>
                            <label htmlFor="deviceFunction">온도 : </label>{' '}
                            <select name="deviceFunction">
                                <option value="18">18도</option>
                                <option value="19">19도</option>
                                <option value="20">20도</option>
                                <option value="21">21도</option>
                                <option value="22">22도</option>
                                <option value="23">23도</option>
                                <option value="24">24도</option>
                                <option value="25">25도</option>
                                <option value="26">26도</option>
                            </select>
                        </FormGroup>
                        <FormGroup>
                            <label htmlFor="device">연결 장치 : </label>{' '}
                            <select name="device">
                                <option value="aircompir">에어컨 리모컨</option>
                                <option value="aircompir2">TV 리모컨</option>
                            </select>
                        </FormGroup>
                    </ModalBody>
                )
                break;
            default:
                break;
        }
    }

    render() {
        const { funParamState } = this.props;
        return (
            <div>
                <Modal isOpen={funParamState} toggle={this.toggle}
                >
                    <ModalHeader toggle={this.toggle}>
                        파라미터 추가
                    </ModalHeader>
                    <Form action="" className="paramForm" onSubmit={this.addFunction}>
                        {this.modalBody()}
                        <ModalFooter>
                            <Button type="submit">추가</Button>{' '}
                            <Button type="button" color="secondary" onClick={this.toggle}>취소</Button>
                        </ModalFooter>
                    </Form>

                </Modal>
            </div>
        );
    }
}

export default withRouter(
    connect((state) => ({
        functionList: state.functionList.functionList,
        funParamOpen: state.functionList.funParamOpen,
        scenarioId: state.board.scenarioId,
        funParamClose: state.functionList.funParamClose,
        paramModalFunctionName: state.functionList.paramModalFunctionName,
    })
    )(FunctionParam));
