import React, {Component} from 'react';
import {Route} from 'react-router-dom';
import ScenarioListPage from "../pages/ScenarioListPage";
import SignUpPage from "../pages/SignUpPage";
import {Container} from "reactstrap";
import ScenarioBoardPage from "../pages/ScenarioBoardPage";
import SignInPage from "../pages/SignInPage";
import WithAuth from "./withAuth";


class App extends Component {
    render() {
        return (
            <div>
                <Container>
                  <Route exact path="/" component={SignInPage}/>
                  <Route exact path="/signUp" component={SignUpPage}/>
                </Container>
                <Route exact path="/main" component={ScenarioListPage}/>
                <Route exact path="/board" component={ScenarioBoardPage}/>
            </div>
        );
    }
}

export default App;