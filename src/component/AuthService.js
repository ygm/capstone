import decode from 'jwt-decode';

export default class AuthService {
  // Initializing important variables
  constructor() {
    AuthService.getProfile = AuthService.getProfile.bind(this)
  }

  static loggedIn() {
    // Checks if there is a saved token and it's still valid
    const token = AuthService.getToken(); // GEtting token from localstorage
    return !!token && !AuthService.isTokenExpired(token) // handwaiving here
  }

  static isTokenExpired(token) {
    try {
      const decoded = decode(token);
      return decoded.exp < Date.now() / 1000;
    }
    catch (err) {
      return false;
    }
  }

  static getToken() {
    // Retrieves the user token from localStorage
    return localStorage.getItem('accessToken')
  }

  static logout() {
    // Clear user token and profile data from localStorage
    localStorage.removeItem('accessToken');
    localStorage.removeItem('refreshToken');
    localStorage.removeItem('expiredIn');
  }

  static getProfile() {
    // Using jwt-decode npm package to decode the token
    return decode(AuthService.getToken());
  }
}