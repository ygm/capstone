import React from 'react';
import "./BoardItem.css";
import {Col, Row} from "reactstrap";
import axios from "axios";
import {baseURL} from "../../lib/api";
import {UncontrolledTooltip} from "reactstrap";
import {BoardActions} from "../../store/actionCreators";
import ModBoardItem from "./ModBoardItem";
import ModDetailBoardItem from './ModDetailBoardItem';

const BoardItem = (props) => {

    const {
        plusClick,
        functions,
        scenarioName,
        scenarioId,
        modBoardState,
        modBoardOpen,
        modalId,
        boardItemRemove,
        boardItemMod,
        modDetailBoardState,
        modalFunctionType,
        history,
    } = props;

    const modClick = (functionId, functionType) => {
        if (modBoardOpen) {
            BoardActions.modBoardClose();
        } else {
            BoardActions.modBoardOpen({functionId, functionType});
        }
    };

    const functionsView = functions.map(
        (value, index) => (
            <li key={index}>
                <img id={"tool" + index.toString()}
                     src={value.functionName==='time-light' ? require('../../static/img/light.png') : (value.functionName==='time-fan' ? require('../../static/img/fan.png') :require('../../static/img/airconditioner.png'))}
                     alt=""
                     className="functionsView_img" onClick={() => modClick(value._id, value.functionName)}/>
                <UncontrolledTooltip placement="top" target={"tool" + index.toString()}>
                    {value.params.time} <br/>
                    {value.params.command}<br/>
                    {value.params.device}
                </UncontrolledTooltip>
            </li>
        )
    );
    const start = async (e) => {
        e.preventDefault();
        try {
            const accessToken = localStorage.getItem("accessToken");
            const response = await axios.post(baseURL + '/board/play', {
                scenarioId: scenarioId,
                functionList: functions
            }, {
                headers: {
                    Authorization: "Bearer " + accessToken
                }
            });

            if (response.status === 200) {
                console.log(response);
                alert("실행중입니다");
            }
        } catch (error) {
            console.log(error);
        }
    };
    const stop = async (e) => {
        e.preventDefault();
        try {
            const accessToken = localStorage.getItem("accessToken");
            const response = await axios.delete(baseURL + '/board/stop', {
                headers: {
                    Authorization: "Bearer " + accessToken
                },
                data: {
                    scenarioId: scenarioId
                }
            });

            if (response.status === 200) {
                console.log(response);
                alert("실행이 중지되었습니다");
            }
        } catch (error) {
            console.log(error);
        }
    };
    return (
        <div className="wrapper">
            <header>
                <h1>{scenarioName}</h1>
            </header>
            <ul className={"board"}>
                {functionsView}
                <li onClick={plusClick}>+</li>
            </ul>
            <footer>
                <Row className={"btnRow"}>
                    <Col sm="12" md={{size: 6, offset: 3}}>
                        <div className='triangle right' onClick={start}></div>
                        {' '}
                        <div className='rectangle stop' onClick={stop}></div>
                    </Col>
                </Row>
            </footer>
            <ModBoardItem functionId={modalId} modBoardState={modBoardState} boardItemRemove={boardItemRemove} boardItemMod={boardItemMod}/>
            <ModDetailBoardItem
                history={history}
                scenarioId={scenarioId}
                functions={functions}
                functionId={modalId}
                functionType={modalFunctionType}
                modBoardState={modDetailBoardState} />
        </div>
    );
};

export default BoardItem;