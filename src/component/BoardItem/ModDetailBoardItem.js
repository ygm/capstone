import React, { Component } from 'react';
import { Button, Form, FormGroup, Modal, ModalBody, ModalHeader, ModalFooter } from "reactstrap";
import { BoardActions } from "../../store/actionCreators";
import Axios from 'axios';
import { baseURL } from '../../lib/api';


class ModDetailBoardItem extends Component {
    modToggle = () => {
        BoardActions.modDetailBoardClose();
    };

    modify = async (e) => {
        e.preventDefault();

        const hour = e.target.hour.value;
        const deviceFunction = e.target.deviceFunction.value;
        const device = e.target.device.value;

        try {
            const accessToken = localStorage.getItem("accessToken");

            const response = await Axios.put(baseURL + '/board', {
                scenarioId: this.props.scenarioId,
                functionId: this.props.functionId,
                params: {
                    time: hour,
                    command: deviceFunction,
                    device: device
                }
            }, {
                headers: {
                    Authorization: "Bearer " + accessToken
                }
            });

            if (response.status === 200) {
                BoardActions.modBoardClose();
                BoardActions.modDetailBoardClose();
                this.props.history.push('/');
                this.props.history.push('/board');
            }
        } catch (error) {
            console.log(error.message);
        }
    }

    bodyForm = () => {
        const { functionType, functionId, functions } = this.props;
        const functionInfo = functions.find((value) => {
            return value._id === functionId;
        })
        switch (functionType) {
            case 'time-light':
                return (
                    <FormGroup>
                        <FormGroup>
                            <label htmlFor="hour">시간 : </label>{' '}
                            <input type="time" id="hour" name="hour" defaultValue={functionInfo.params.time}
                                required />
                        </FormGroup>
                        <FormGroup>
                            <label htmlFor="deviceFunction">기능 : </label>{' '}
                            <select name="deviceFunction" defaultValue={functionInfo.params.command}>
                                <option value="기능선택">기능선택</option>
                                <option value="on">On</option>
                                <option value="off">Off</option>
                            </select>
                        </FormGroup>
                        <FormGroup>
                            <label htmlFor="device">연결 장치 : </label>{' '}
                            <select name="device" defaultValue={functionInfo.params.device}>
                                <option value="장치선택">장치선택</option>
                                <option value="장치1">장치1</option>
                                <option value="장치2">장치2</option>
                                <option value="장치3">장치3</option>
                                <option value="장치4">장치4</option>
                            </select>
                        </FormGroup>
                    </FormGroup>
                );
                break;
            case 'time-fan':
                return (
                    <FormGroup>
                        <FormGroup>
                            <label htmlFor="hour">시간 : </label>{' '}
                            <input type="time" id="hour" name="hour" defaultValue={functionInfo.params.time}
                                required />
                        </FormGroup>
                        <FormGroup>
                            <label htmlFor="deviceFunction">기능 : </label>{' '}
                            <select name="deviceFunction" defaultValue={functionInfo.params.command}>
                                <option value="기능선택">기능선택</option>
                                <option value="on">On</option>
                                <option value="off">Off</option>
                            </select>
                        </FormGroup>
                        <FormGroup>
                            <label htmlFor="device">연결 장치 : </label>{' '}
                            <select name="device" defaultValue={functionInfo.params.device}>
                                <option value="장치선택">장치선택</option>
                                <option value="장치1">장치1</option>
                                <option value="장치2">장치2</option>
                                <option value="장치3">장치3</option>
                                <option value="장치4">장치4</option>
                            </select>
                        </FormGroup>
                    </FormGroup>
                )
                break;
            case 'time-airTemp':
                return (
                    <FormGroup>
                        <FormGroup>
                            <label htmlFor="hour">시간 : </label>{' '}
                            <input type="time" id="hour" name="hour" defaultValue={functionInfo.params.time}
                                required />
                        </FormGroup>
                        <FormGroup>
                            <label htmlFor="deviceFunction">온도 : </label>{' '}
                            <select name="deviceFunction">
                                <option value="18">18도</option>
                                <option value="19">19도</option>
                                <option value="20">20도</option>
                                <option value="21">21도</option>
                                <option value="22">22도</option>
                                <option value="23">23도</option>
                                <option value="24">24도</option>
                                <option value="25">25도</option>
                                <option value="26">26도</option>
                            </select>
                        </FormGroup>
                        <FormGroup>
                            <label htmlFor="device">연결 장치 : </label>{' '}
                            <select name="device">
                                <option value="aircompir">에어컨 리모컨</option>
                                <option value="aircompir2">TV 리모컨</option>
                            </select>
                        </FormGroup>
                    </FormGroup>
                );
                break;
            default:
                break;
        }
    }

    render() {
        const { modBoardState, functionId, functionType } = this.props;

        return (

            <div>
                <Modal isOpen={modBoardState} toggle={this.modToggle}
                // className={this.props.className}
                >
                    <ModalHeader toggle={this.modToggle}>
                        수정하기
                    </ModalHeader>
                    <Form action="" className="paramForm" onSubmit={this.modify}>
                        <ModalBody>
                            {this.bodyForm()}
                        </ModalBody>
                        <ModalFooter>
                            <Button type="submit">수정</Button>{' '}
                            <Button type="button" color="secondary" onClick={this.modToggle}>취소</Button>
                        </ModalFooter>
                    </Form>
                </Modal>
            </div>
        );

    }
}

export default ModDetailBoardItem;
