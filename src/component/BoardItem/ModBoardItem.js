import React, {Component} from 'react';
import {Button, Form, Modal, ModalBody, ModalHeader,ModalFooter} from "reactstrap";
import {BoardActions} from "../../store/actionCreators";

// import BoardItem from "./BoardItem";

class ModBoardItem extends Component {
    modToggle = () => {
        const {modBoardState} = this.props;

        if (modBoardState) {
            BoardActions.modBoardClose();
        } else {
            BoardActions.modBoardOpen();
        }
    };


    render() {
        const {modBoardState, boardItemRemove, boardItemMod, functionId} = this.props;

        return (

            <div>
                <Modal isOpen={modBoardState} toggle={this.modToggle}
                    // className={this.props.className}
                >
                    <ModalHeader toggle={this.modToggle}>
                        수정 / 삭제
                    </ModalHeader>
                    <ModalBody>
                        <Form action="" className="paramForm">
                            <Button type="button" onClick={boardItemMod}>수정</Button>{' '}
                            <Button type="button" color="secondary" onClick={() => boardItemRemove(functionId)}>삭제</Button>
                        </Form>
                    </ModalBody>

                </Modal>
            </div>
        );

    }
}

export default ModBoardItem;
