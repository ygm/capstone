import React, {Component} from 'react';
import BoardItemContainer from "../container/BoardItemContainer";

class Board extends Component {
    render() {
        const { plusClick } = this.props;

        return (
          <BoardItemContainer plusClick={plusClick}/>
        );
    }
}

export default Board;