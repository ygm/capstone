import React from 'react';
import {Navbar, NavbarBrand} from 'reactstrap';

const Header = () => {
    return (
      <div>
          <Navbar color="dark" light fixed="top">
              <NavbarBrand href="/main" style={{color: "white"}}>IoT Designer</NavbarBrand>
          </Navbar>
      </div>
    );
};

export default Header;