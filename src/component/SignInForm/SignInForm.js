import React from 'react';
import FormGroup from "reactstrap/es/FormGroup";
import Input from "reactstrap/es/Input";
import Button from "reactstrap/es/Button";
import Form from "reactstrap/es/Form";
import "./SignInForm.css";
import {Row} from "reactstrap";
import Col from "reactstrap/es/Col";

const SignInForm = (props) => {
    const {signIn, toSignUpPage} = props;
    return (
      <div className="SignUp-col">
        <Row >
          <Col sm="12" md={{size: 4, offset: 4}} >
            <header className="login__header">
              <h3 className="text-center login__title">Login</h3>
            </header>
            <Form className="signInForm align-middle" onSubmit={signIn}>
              <div className="login__body">
                <FormGroup className="form__field">
                  <label htmlFor="log_id">ID</label>
                  <Input
                    id="log_id"
                    placeholder="ID"
                    name="username"
                  />
                </FormGroup>
                <FormGroup className="form__field">
                  <label htmlFor="log_pwd" >PASSWORD</label>
                  <Input
                    id="log_pwd"
                    type="password"
                    placeholder="PASSWORD"
                    name="password"
                  />
                </FormGroup>
              </div>
              <div className="login__footer">
                <Button type="submit" size="md"
                        className="btnC">LOGIN</Button>
                <Button type="button" size="md" onClick={toSignUpPage}
                        className="btnC">SIGN UP</Button>
              </div>
            </Form>
          </Col>
        </Row>
      </div>
    );
};

export default SignInForm;