import React, {Component} from 'react';
import Header from "../component/Header";
import ScenarioList from "../container/ScenarioList";
import withAuth from "../component/withAuth";

class ScenarioListPage extends Component {

    render() {
        return (
          <div>
              <Header/>
              <ScenarioList/>
          </div>
        );
    }
}

export default withAuth(ScenarioListPage);