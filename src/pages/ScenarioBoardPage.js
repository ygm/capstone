import React, {Component} from 'react';
import ScenarioBoard from "../container/ScenarioBoard";
import withAuth from "../component/withAuth";

class ScenarioBoardPage extends Component {
    render() {
        return (
            <ScenarioBoard/>
        );
    }
}

export default withAuth(ScenarioBoardPage);