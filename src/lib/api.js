import axios from "axios";

console.log('mode: ', process.env.REACT_APP_START_MODE);

export const baseURL = process.env.REACT_APP_START_MODE === 'production' ? 
  'http://ec2-13-125-248-36.ap-northeast-2.compute.amazonaws.com/api' : 'http://localhost:4000';

export const tokenCheck = async () => {
  const token = localStorage.getItem("accessToken");

  try {
    const response = await axios.get(baseURL + '/auth/check', {
      headers: {
        Authorization: "Bearer " + token
      },
    });

    return true;
  } catch (e) {
    console.log(e);
    return false;
  }
};