// 편의상, 나중에 액션 생성 함수들을 미리 바인딩해서 내보냄

import {bindActionCreators} from "redux";
import {actionCreators as LoginActionCreator} from './modules/login';
import {actionCreators as ScenarioActionCreator} from './modules/scenario';
import {actionCreators as FunctionActionCreator} from './modules/functionList';
import {actionCreators as BoardActionCreator} from './modules/board';

import store from './index';

const { dispatch } = store;

export const LoginActions = bindActionCreators(LoginActionCreator, dispatch);
export const ScenarioActions = bindActionCreators(ScenarioActionCreator, dispatch);
export const FunctionActions = bindActionCreators(FunctionActionCreator, dispatch);
export const BoardActions = bindActionCreators(BoardActionCreator, dispatch);
