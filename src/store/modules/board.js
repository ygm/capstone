import { createAction, handleActions } from 'redux-actions';
import produce from 'immer';

const FUN_LIST_OPEN = 'board/FUN_LIST_OPEN';
const FUN_LIST_CLOSE = 'board/FUN_LIST_CLOSE';
const SCENARIO_NAME = 'board/SCENARIO_NAME';
const FUNCTIONS_ADD = 'board/FUNCTIONS_ADD';
const FUNCTIONS_LIST = 'board/FUNCTIONS_LIST';
const SCENARIO_ID = 'board/SCENARIO_ID';
const MOD_BOARD_OPEN = 'board/MOD_BOARD_OPEN';
const MOD_BOARD_CLOSE = 'board/MOD_BOARD_CLOSE';
const MOD_DETAIL_BOARD_OPEN = 'board/MOD_DETAIL_BOARD_OPEN';
const MOD_DETAIL_BOARD_CLOSE = 'board/MOD_DETAIL_BOARD_CLOSE';

const funListOpen = createAction(FUN_LIST_OPEN);
const funListClose = createAction(FUN_LIST_CLOSE);
const modBoardOpen = createAction(MOD_BOARD_OPEN, (payload)=> payload);
const modBoardClose = createAction(MOD_BOARD_CLOSE);
const modDetailBoardOpen = createAction(MOD_DETAIL_BOARD_OPEN);
const modDetailBoardClose = createAction(MOD_DETAIL_BOARD_CLOSE);
const scenarioName = createAction(SCENARIO_NAME, (payload)=> payload);
const functionsAdd = createAction(FUNCTIONS_ADD, (payload)=> payload);
const functionsList = createAction(FUNCTIONS_LIST, (payload)=> payload);
const scenarioId = createAction(SCENARIO_ID, (payload)=> payload);


const initialState = {
  funListOpen: false,
  scenarioName: null,
  scenarioId: null,
  functions: [],
  modBoardState: false,
  modalId: null,
  modalFunctionType: null,
  modDetailBoardState: false,
};

export const actionCreators = {
  funListOpen,
  funListClose,
  modBoardOpen,
  modBoardClose,
  scenarioName,
  functionsAdd,
  functionsList,
  scenarioId,
  modDetailBoardOpen,
  modDetailBoardClose,
};

export default handleActions({
  [FUN_LIST_OPEN]: (state, action) => {
    return produce(state, (draft) => {
      draft.funListOpen = true;
    });
  },
  [FUN_LIST_CLOSE]: (state, action) => {
    return produce(state, (draft) => {
      draft.funListOpen = false;
    });
  },
  [MOD_BOARD_OPEN]: (state, action) => {
    return produce(state, (draft) => {
      if (!action) return;
      draft.modBoardState = true;
      draft.modalId = action.payload.functionId;
      draft.modalFunctionType = action.payload.functionType;
    });
  },
  [MOD_BOARD_CLOSE]: (state, action) => {
    return produce(state, (draft) => {
      draft.modBoardState = false;
      draft.modalId = null;
      draft.modalFunctionType = null;
    });
  },
  [MOD_DETAIL_BOARD_OPEN]: (state, action) => {
    return produce(state, (draft) => {
      draft.modDetailBoardState = true;
    });
  },
  [MOD_DETAIL_BOARD_CLOSE]: (state, action) => {
    return produce(state, (draft) => {
      draft.modDetailBoardState = false;
    });
  },
  [SCENARIO_NAME]: (state, action) => {
    return produce(state, (draft) => {
      if (!action) return;
      draft.scenarioName = action.payload;
    });
  },
  [FUNCTIONS_ADD]: (state, action) => {
    return produce(state, (draft) => {
      if (!action) return;
      draft.functions.push(action.payload);
    });
  },
  [FUNCTIONS_LIST]: (state, action) => {
    return produce(state, (draft) => {
      if (!action) return;
      draft.functions= action.payload;
    });
  },
  [SCENARIO_ID]: (state, action) => {
      return produce(state, (draft) => {
        if (!action) return;
        draft.scenarioId= action.payload;
      });
  },

}, initialState);