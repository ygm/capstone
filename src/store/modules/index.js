import { combineReducers } from 'redux';
import login from './login';
import scenario from "./scenario";
import functionList from "./functionList";
import board from "./board";

export default combineReducers({
  login,
  scenario,
  functionList,
  board
});