import { createAction, handleActions } from 'redux-actions';
import produce from 'immer';

const SCENARIO_LIST = 'scenario/SCENARIO_LIST';
const ADD_SCENARIO = 'scenario/ADD_SCENARIO';
const ADD_MODAL_OPEN = 'scenario/ADD_MODAL_OPEN';
const ADD_MODAL_CLOSE = 'scenario/ADD_MODAL_CLOSE';
const REMOVE_MODAL_CLOSE = 'scenario/REMOVE_MODAL_CLOSE';
const REMOVE_MODAL_OPEN = 'scenario/REMOVE_MODAL_OPEN';

const scenarioList = createAction(SCENARIO_LIST, (payload) => payload);
const addScenario = createAction(ADD_SCENARIO, (payload) => payload);
const addModalOpen = createAction(ADD_MODAL_OPEN);
const addModalClose = createAction(ADD_MODAL_CLOSE);
const removeModalOpen = createAction(REMOVE_MODAL_OPEN);
const removeModalClose = createAction(REMOVE_MODAL_CLOSE);

const initialState = {
  scenarioList: [
    {
      name: null,
      _id: null
    }
  ],
  scenarioInput: '',
  addModalState: false,
  removeModalState: false,
};

export const actionCreators = {
  scenarioList,
  addScenario,
  addModalOpen,
  addModalClose,
  removeModalOpen,
  removeModalClose
};

export default handleActions({
  [SCENARIO_LIST]: (state, action) => {
    return produce(state, (draft) => {
      if (!action) return;
      draft.scenarioList = action.payload;
    });
  },
  [ADD_SCENARIO]: (state, action) => {
    return produce(state, (draft) => {
      if (!action) return;
      draft.scenarioInput = action.payload;
    });
  },
  [ADD_MODAL_OPEN]: (state, action) => {
    return produce(state, (draft) => {
      draft.addModalState = true;
    });
  },
  [ADD_MODAL_CLOSE]: (state, action) => {
    return produce(state, (draft) => {
      draft.addModalState = false;
    });
  },
  [REMOVE_MODAL_OPEN]: (state, action) => {
    return produce(state, (draft) => {
      draft.removeModalState = true;
    });
  },
  [REMOVE_MODAL_CLOSE]: (state, action) => {
    return produce(state, (draft) => {
      draft.removeModalState = false;
    });
  },
}, initialState);