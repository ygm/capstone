import { createAction, handleActions } from 'redux-actions';
import produce from 'immer';

const FUNCTION_LIST = 'function/FUNCTION_LIST';
const FUN_PARAM_OPEN = 'function/FUN_PARAM_OPEN';
const FUN_PARAM_CLOSE = 'function/FUN_PARAM_CLOSE';

const functionList = createAction(FUNCTION_LIST, (payload)=> payload);
const funParamOpen = createAction(FUN_PARAM_OPEN, (payload) => payload);
const funParamClose = createAction(FUN_PARAM_CLOSE);

const initialState = {
  functionList: [],
  funParamState: false,
  paramModalFunctionName: '',
};

export const actionCreators = {
  functionList,
  funParamOpen,
  funParamClose
};

export default handleActions({
  [FUNCTION_LIST]: (state, action) => {
    return produce(state, (draft) => {
      if (!action) return;
      draft.functionList = action.payload;
    });
  },
  [FUN_PARAM_OPEN]: (state, action) => {
    return produce(state, (draft) => {
      if (!action) return;
      draft.funParamState = true;
      draft.paramModalFunctionName = action.payload;
    });
  },
  [FUN_PARAM_CLOSE]: (state, action) => {
    return produce(state, (draft) => {
      draft.funParamState = false;
    });
  },
}, initialState);