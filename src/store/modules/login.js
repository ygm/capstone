// 카운터 관련 상태 로직
import { createAction, handleActions } from 'redux-actions';
import produce from 'immer';

// 액션 타입을 정의해줍니다.
const LOGIN_SUCCESS = 'user/LOGIN_SUCCESS';

// 액션 생성 함수를 만듭니다.
//

// {
//   'type': 'user/LOGIN_SUCCESS',
//   payload: payload
// }
const loginSuccess = createAction(LOGIN_SUCCESS, (payload) => payload);

// 모듈의 초기 상태를 정의합니다.
const initialState = {
  user: "",
  isLoggedIn: false,
};

export const actionCreators = {
  loginSuccess,
};

export default handleActions({
  [LOGIN_SUCCESS]: (state, action) => {
    return produce(state, (draft) => {
      if (!action) return;
      draft.user = action.payload;
      draft.isLoggedIn = true;
    });
  }
}, initialState);