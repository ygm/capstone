import React, {Component} from 'react';
import SignInForm from "../component/SignInForm/SignInForm";
import {connect} from "react-redux";
import {withRouter} from "react-router-dom";
import axios from "axios";
import {baseURL} from "../lib/api";
import {LoginActions} from "../store/actionCreators";

class SignIn extends Component {

  signIn = async (e) => {
    e.preventDefault();

    const username = e.target.username.value;
    const password = e.target.password.value;

    console.log(username, password);
    if (username === '' || password === '') {
      alert("모든 정보를 입력해주세요");
    } else {
      try {
        const response = await axios.post(baseURL + '/auth/login', {
          grant_type: "password",
          username: username,
          password: password
        });

        if (response.status === 200) {
          localStorage.setItem("accessToken", response.data.accessToken);
          localStorage.setItem("refreshToken", response.data.refreshToken);
          localStorage.setItem("expiresIn", response.data.expiresIn);
          await LoginActions.loginSuccess(username);
          this.props.history.push('/main');
        }
      } catch (error) {
        alert("아이디와 비밀번호를 확인해주세요.");
      }
    }
  };

  toSignUpPage = (e) => {
    this.props.history.push('/signUp');
  };

  render() {
    return (
      <div className="body">
        <SignInForm
          signIn={this.signIn}
          toSignUpPage={this.toSignUpPage}
        />
      </div>
    );
  }
}

export default withRouter(
  connect((state) => ({
      user: state.login.user
    })
  )(SignIn));