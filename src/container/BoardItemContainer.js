import React, { Component } from 'react';
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import BoardItem from "../component/BoardItem/BoardItem";
import axios from "axios";
import { baseURL } from "../lib/api";
import { BoardActions } from "../store/actionCreators";

class BoardItemContainer extends Component {

    boardItemRemove = async (id) => {
        const {scenarioId} = this.props;
        if (window.confirm("삭제하시겠습니까?")) {
            try {
                const accessToken = localStorage.getItem("accessToken");
                const response = await axios.delete(baseURL + '/board', {
                    headers: {
                        Authorization: "Bearer " + accessToken
                    },
                    data: {
                        scenarioId: scenarioId,
                        functionId: id
                    },
                });

                if (response.status === 200) {
                    await BoardActions.modBoardClose();
                    this.props.history.push('/');
                    this.props.history.push('/board');
                    // window.location.reload()
                }

            } catch (e) {
                console.log(e.response);
            }

        } else {
            await BoardActions.modBoardClose();
        }
    };

    boardItemMod = async () => {
        await BoardActions.modDetailBoardOpen();
    };

    render() {
        return (
            <BoardItem
                history={this.props.history}
                functions={this.props.functions}
                plusClick={this.props.plusClick}
                scenarioName={this.props.scenarioName}
                scenarioId={this.props.scenarioId}
                modBoardState={this.props.modBoardState}
                modBoardOpen={this.props.modBoardOpen}
                modalId={this.props.modalId}
                boardItemRemove={this.boardItemRemove}
                boardItemMod={this.boardItemMod}
                modDetailBoardState={this.props.modDetailBoardState}
                modalFunctionType={this.props.modalFunctionType}
            />
        );
    }
}

export default withRouter(
    connect((state) => ({
        scenarioName: state.board.scenarioName,
        functions: state.board.functions,
        scenarioId: state.board.scenarioId,
        modBoardState: state.board.modBoardState,
        modBoardOpen: state.board.modBoardOpen,
        modalId: state.board.modalId,
        modalFunctionType: state.board.modalFunctionType,
        modDetailBoardState: state.board.modDetailBoardState,
    }))(BoardItemContainer));
