import React, {Component} from 'react';
import axios from "axios";
import 'bootstrap/dist/css/bootstrap.min.css';
import Col from "reactstrap/es/Col";
import {Row} from "reactstrap";
import SignUpForm from "../component/SignUpForm/SignUpForm";
import {withRouter} from "react-router-dom";
import {baseURL} from "../lib/api";

class SignUp extends Component {

  signUp = async (e) => {
    e.preventDefault();

    const username = e.target.username.value;
    const password = e.target.password.value;
    
    alert(' Id is ' + username + ' Password is ' + password);
    if (username === '' || password === '') {
      alert("모든 정보를 입력해주세요");
    } else {
      try {
        const response = await axios.post(baseURL+ '/auth/register', {
          username: username,
          password: password
        });

        if (response.status === 200) {
          console.log(response);
          alert('회원가입 되었습니다.');
          this.props.history.push('/signIn');
        }
      } catch (error) {
        console.log(error);
        // todo 회원가입 실패
      }
    }
  };

  toSignInPage = (e) => {
    this.props.history.push('/');
  };


  render() {
    return (
      <div className={"SignUp-col"}>
        <Row>
          <Col sm="12" md={{size: 4, offset:4}}>
            <header className="SignUp__header">
              <h3 className="text-center SignUp__title">Sign Up</h3>
            </header>
            <SignUpForm signUp={this.signUp} toSignInPage={this.toSignInPage}/>
          </Col>
        </Row>
      </div>
    );
  }
}

export default withRouter(SignUp);

