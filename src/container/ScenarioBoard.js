import React, {Component} from 'react';
import FunctionListSidebar from "../component/FunctionListSidebar/FunctionListSidebar";
import Board from "../component/Board";
import {withRouter} from "react-router-dom";
import {connect} from "react-redux";
import {BoardActions, FunctionActions, ScenarioActions} from "../store/actionCreators";
import Header from "../component/Header";
import axios from "axios";
import {baseURL} from "../lib/api";

class ScenarioBoard extends Component {

    // componentDidMount() {
        // axios > res
        // FunctionActions.functionList(res.data);
    // }
    async componentDidMount(){
        try {
            console.log(this.props.scenarioId);
            const accessToken = localStorage.getItem("accessToken");
            const response = await axios.get(baseURL+'/board/'+ this.props.scenarioId, {
                headers: {
                    Authorization: "Bearer " + accessToken
                },
            });

            if (response.status === 200) {
                console.log(response.data);
                await BoardActions.functionsList(response.data.functionList)
            }
        }catch (e) {
            console.log('error : ', e);
        }
        try {
            const accessToken = localStorage.getItem("accessToken");
            const response = await axios.get(baseURL+'/scenario', {
                headers: {
                    Authorization: "Bearer " + accessToken
                },
            });

            if (response.status === 200) {
                console.log(response.data);
                await FunctionActions.functionList(response.data)
            }
        }catch (e) {
            console.log('error : ', e);
        }
    }

    plusClick = (e) => {
        const {funListOpen} = this.props;

        if (funListOpen) {
            BoardActions.funListClose();
        } else {
            BoardActions.funListOpen();
        }
    };

    render() {
        // console.log(this.props.functions);
        return (
            <div>
                <Header/>
                <FunctionListSidebar funListOpen={this.props.funListOpen}/>
                <Board plusClick={this.plusClick}/>
            </div>
        );
    }
}

export default withRouter(
  connect((state) => ({
      funListOpen: state.board.funListOpen,
      scenarioId: state.board.scenarioId
    })
  )(ScenarioBoard));