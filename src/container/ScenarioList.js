import React, {Component} from 'react';
import "../styles/card.css";
import ScenarioListItem from "../component/ScenarioList/ScenarioListItem";
import axios from "axios";
import {withRouter} from "react-router-dom";
import {connect} from "react-redux";
import {baseURL} from "../lib/api";
import {BoardActions, ScenarioActions} from "../store/actionCreators";


class ScenarioList extends Component {

    async componentDidMount(){
        try {
            const accessToken = localStorage.getItem("accessToken");
            const response = await axios.get(baseURL+'/scenario', {
                headers: {
                    Authorization: "Bearer " + accessToken
                },
            });

            if (response.status === 200) {
                await ScenarioActions.scenarioList(response.data)
            }
        }catch (e) {
            console.log('error : ', e);
        }
    }

    handleChange = async (e) => {
        console.log(e.target.value);
        await ScenarioActions.addScenario(e.target.value);
    };

    handleInsert = async (e) => {
        const {scenarioInput} = this.props;

        if (scenarioInput === '') {
            alert("모든 정보를 입력해주세요");
        } else {
            try {
                console.log(scenarioInput);
                const accessToken = localStorage.getItem("accessToken");
                const response = await axios.post(baseURL+'/scenario', {
                    name: scenarioInput
                },{
                    headers: {
                        Authorization: "Bearer " + accessToken
                    },
                });
                console.log(response);

                if (response.status === 200) {
                    console.log("ok");
                    await ScenarioActions.addModalClose();
                    this.props.history.push('/');
                    this.props.history.push('/main');
                }
            } catch (error) {
                alert("실패");
                console.log(error);
                await ScenarioActions.addScenario('');
            }

        }
    };

    handleRemove = async (_id) => {
        if (window.confirm("삭제하시겠습니까?")) {
            try {
                const accessToken = localStorage.getItem("accessToken");
                console.log(accessToken);
                const response = await axios.delete(baseURL+'/scenario', {
                    headers: {
                        Authorization: "Bearer " + accessToken
                    },
                    data: {
                        _id
                    },
                });

                if (response.status === 200) {
                    console.log("ok");
                    await ScenarioActions.removeModalClose();
                    this.props.history.push('/');
                    this.props.history.push('/main');
                }

            } catch (e) {
                console.log("error",e);
            }

        } else {
            await ScenarioActions.removeModalClose();
        }
    };


    goBoard = (name, id) => {
        BoardActions.scenarioName(name);
        BoardActions.scenarioId(id);
        this.props.history.push('/board');
    };

    render() {
        return (
          <ScenarioListItem
            handleChange = {this.handleChange}
            handleInsert = {this.handleInsert}
            handleRemove = {this.handleRemove}
            goBoard = {this.goBoard}
            scenarios={this.props.scenario}
            addModalState={this.props.addModalState}
          />
        );
    }
}

export default withRouter(
  connect((state) => ({
      scenario: state.scenario.scenarioList,
      scenarioInput: state.scenario.scenarioInput,
      addModalState: state.scenario.addModalState,
      removeModalState: state.scenario.removeModalState,
    })
  )(ScenarioList));